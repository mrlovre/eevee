#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

echo "Creating dir '$1'..."
mkdir -p $1
echo "Copying files from '$DIR' to '$1'"
cp $DIR/* $1
echo "Done."
